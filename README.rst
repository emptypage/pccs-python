pccs-python
===========

PCCS library for Python
-----------------------

PCCS (Practical Color Coordinate System / 日本色研配色体系) is a color
notation system which is developed by Japan Color Research Institute
(日本色彩研究所) in 1966.

This library provides conversions between PCCS and Munsell color system
notation based on a scientific algorythm.


Requirements
------------

- Python 3.9 or newer


Examples
--------


References
----------

