.. pccs documentation master file, created by
   sphinx-quickstart on Sat Sep 11 00:46:35 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to pccs's documentation!
================================

.. toctree::
   :maxdepth: 4
   :caption: Contents:

   pccs


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
