"""PCCS (Practical Color Coordinate System) for Python core module

References:

The algolythm of the conversion between PCCS and Munsell color system
is based on the paper bellow:

-   Kobayashi, Mituo; and Yosiki, Kayoko. 2001. Mathematical Relation
    among PCCS Tones, PCCS Color Attributes and Munsell Color
    Attributes. Nihon Shikisai Gakkai Shi [Color Science Association of
    Japan] 25 (4), 249-261.
"""


__all__ = [
    'PCCS_HUE_CODES', 'PCCS_TONE_COORDS', 'PCCS_TONE_NAMES',
    'PCCS_HUE_NAME_PATTERN', 'PCCS_GREY_LONG_PATTERN',
    'PCCS_COLOUR_LONG_PATTERN', 'PCCS_GREY_PATTERN',
    'PCCS_COLOUR_PATTERN',
    'pccs_tone_to_lightness_and_saturation',
    'pccs_lightness_and_saturation_to_tone',
    'parse_pccs_colour', 'pccs_hue_code_to_float',
    'pccs_colour_to_pccs_specification',
    'pccs_specification_to_pccs_colour'
]


import re


from colour.constants import FLOATING_POINT_NUMBER_PATTERN
from colour.notation.munsell import (normalize_munsell_specification,
                                     munsell_specification_to_munsell_colour,
                                     munsell_colour_to_munsell_specification)

import numpy as np
import numpy.typing as npt
from numpy import pi, sin, sqrt


from .easy import (pccs_specification_to_munsell_specification,
                   munsell_specification_to_pccs_specification)
from .utils import round_align


PCCS_HUE_CODES = [
    '1:pR',
    '2:R',
    '3:yR',
    '4:rO',
    '5:O',
    '6:yO',
    '7:rY',
    '8:Y',
    '9:gY',
    '10:YG',
    '11:yG',
    '12:G',
    '13:bG',
    '14:BG',
    '15:BG',
    '16:gB',
    '17:B',
    '18:B',
    '19:pB',
    '20:V',
    '21:bP',
    '22:P',
    '23:rP',
    '24:RP',
]
"""List contains all hue codes of PCCS."""

PCCS_TONE_COORDS = {
    # r:    (s, t)
    'p':    (2.0, 8.6),
    'p+':   (3.0, 8.2),
    'ltg':  (2.0, 7.1),
    'g':    (2.0, 4.1),
    'dkg':  (2.0, 2.1),
    'lt':   (5.0, 7.8),
    'lt+':  (6.0, 7.3),
    'sf':   (5.0, 6.3),
    'd':    (5.0, 4.8),
    'dk':   (5.0, 3.0),
    'b':    (8.0, 6.6),
    's':    (8.0, 5.2),
    'dp':   (8.0, 4.1),
    'v':    (9.0, 5.5),
}
PCCS_TONE_NAMES = list(PCCS_TONE_COORDS.keys())

PCCS_HUE_NAME_PATTERN = '|'.join(PCCS_HUE_CODES)
PCCS_GREY_LONG_PATTERN = ('n-(?P<value>{0})'
                          .format(FLOATING_POINT_NUMBER_PATTERN))
PCCS_COLOUR_LONG_PATTERN = ('(?P<hue_name>{0})'
                            '-(?P<lightness>{1})'
                            '-(?P<saturation>{1})s'
                            .format(PCCS_HUE_NAME_PATTERN,
                                    FLOATING_POINT_NUMBER_PATTERN))
PCCS_GREY_PATTERN = 'Gy-(?P<value>{0})'.format(FLOATING_POINT_NUMBER_PATTERN)
PCCS_COLOUR_PATTERN = ('(?P<tone>ltg|dkg|lt\\+?|dk|dp|sf|p\\+?|d|g|b|s|v)'
                       '(?P<hue>{0})'
                       .format(FLOATING_POINT_NUMBER_PATTERN))


def pccs_tone_to_lightness_and_saturation(hue: float, tone: str
                                          ) -> tuple[float, float]:
    """Return lightness and saturation of tone on the given hue.

    Parameters
    ----------
    hue: float
        PCCS hue index. [0-24]
    tone: str
        PCCS tone name witch defined in :const:`PCCS_TONE_NAMES`.

    Returns
    -------
    lightness: float
        PCCS lightness for the hue/tone.
    saturation: float
        PCCS saturation for the hue/tone.


    >>> pccs_tone_to_lightness_and_saturation(2.0, 'p')
    (8.5, 2.0)
    >>> pccs_tone_to_lightness_and_saturation(2.0, 'ltg')
    (7.0, 2.0)
    >>> pccs_tone_to_lightness_and_saturation(22.0, 'dp')
    (2.5, 8.0)
    """

    s, t = PCCS_TONE_COORDS[tone]
    l = t + (0.25 - 0.34 * sqrt(1 - sin((hue - 2) / 12 * pi))) * s
    l = round_align(l, 0.5)
    return l, s


def pccs_lightness_and_saturation_to_tone(pccs_spec: npt.ArrayLike) -> str:
    """Return PCCS tone name from lightness and saturation value on the
    given hue value

    >>> pccs_lightness_and_saturation_to_tone(np.array([12.0, 5.5, 9.0]))
    'v'
    >>> pccs_lightness_and_saturation_to_tone(np.array([2.0, 8.5, 2.0]))
    'p'
    """

    h, l, s = pccs_spec
    t = l - (0.25 - 0.34 * sqrt(1 - sin((h - 2) / 12 * pi))) * s
    distances = [((s0 - s) ** 2 + (t0 - t)) ** 2
                 for s0, t0
                 in PCCS_TONE_COORDS.values()]
    i = distances.index(min(distances))
    return PCCS_TONE_NAMES[i]


def parse_pccs_colour(pccs_colour: str) -> tuple[float, float, float]:
    """Convert PCCS expression to intermediate specification values.

    >>> parse_pccs_colour('v12')
    (12.0, 5.5, 9.0)
    >>> parse_pccs_colour('p2')
    (2.0, 8.5, 2.0)
    >>> parse_pccs_colour('Gy-5.5')
    (0.0, 5.5, 0.0)
    >>> parse_pccs_colour('W')
    (0.0, 9.5, 0.0)
    >>> parse_pccs_colour('Bk')
    (0.0, 1.5, 0.0)
    >>> parse_pccs_colour('12:G-5.5-9s')
    (12.0, 5.5, 9.0)
    >>> parse_pccs_colour('2:R-8.5-2s')
    (2.0, 8.5, 2.0)
    >>> parse_pccs_colour('n-5.5')
    (0.0, 5.5, 0.0)
    >>> parse_pccs_colour('n-9.5')
    (0.0, 9.5, 0.0)
    >>> parse_pccs_colour('n-1.5')
    (0.0, 1.5, 0.0)
    """

    if pccs_colour == 'W':
        return 0.0, 9.5, 0.0

    if pccs_colour == 'Bk':
        return 0.0, 1.5, 0.0

    match = re.match(PCCS_GREY_PATTERN, pccs_colour, flags=re.I)
    if match:
        h = 0.0
        l = float(match.group('value'))
        s = 0.0
        return h, l, s

    match = re.match(PCCS_COLOUR_PATTERN, pccs_colour, flags=re.I)
    if match:
        h = float(match.group('hue'))
        r = match.group('tone')
        l, s = pccs_tone_to_lightness_and_saturation(h, r)
        return h, l, s

    match = re.match(PCCS_GREY_LONG_PATTERN, pccs_colour, flags=re.I)
    if match:
        h = 0.0
        l = float(match.group('value'))
        s = 0.0
        return h, l, s

    match = re.match(PCCS_COLOUR_LONG_PATTERN, pccs_colour, flags=re.I)
    if match:
        h = pccs_hue_code_to_float(match.group('hue_name'))
        l = float(match.group('lightness'))
        s = float(match.group('saturation'))
        return h, l, s

    raise ValueError('invalid PCCS expression')


def pccs_hue_code_to_float(hue_name: str) -> float:
    """Return float hue value from PCCS hue code (e.g. '1:pR').

    >>> pccs_hue_code_to_float('2:R')
    2.0
    >>> pccs_hue_code_to_float('4:rO')
    4.0
    """

    return float(PCCS_HUE_CODES.index(hue_name) + 1)


def pccs_colour_to_pccs_specification(pccs_colour: str) -> npt.ArrayLike:
    """Convert from PCCS color to PCCS intermediate specificaion.
    """

    return np.array(parse_pccs_colour(pccs_colour))


def pccs_specification_to_pccs_colour(pccs_spec: npt.ArrayLike,
                                      long: bool = False) -> str:
    """Convert from PCCS intermediate specification to PCCS color.

    >>> pccs_specification_to_pccs_colour([12.0, 5.5, 9.0])
    'v12'
    >>> pccs_specification_to_pccs_colour([2.0, 8.5, 2.0])
    'p2'
    >>> pccs_specification_to_pccs_colour([0.0, 5.5, 0.0])
    'Gy-5.5'
    >>> pccs_specification_to_pccs_colour([0.0, 9.5, 0.0])
    'W'
    >>> pccs_specification_to_pccs_colour([0.0, 1.5, 0.0])
    'Bk'
    >>> pccs_specification_to_pccs_colour([12.0, 5.5, 9.0], long=True)
    '12:G-5.5-9s'
    >>> pccs_specification_to_pccs_colour([2.0, 8.5, 2.0], long=True)
    '2:R-8.5-2s'
    >>> pccs_specification_to_pccs_colour([0.0, 5.5, 0.0], long=True)
    'n-5.5'
    >>> pccs_specification_to_pccs_colour([0.0, 9.5, 0.0], long=True)
    'n-9.5'
    >>> pccs_specification_to_pccs_colour([0.0, 1.5, 0.0], long=True)
    'n-1.5'
    """

    h, l, s = pccs_spec

    if long:
        if h == s == 0.0:
            return 'n-{0}'.format(l)
        hue_name = PCCS_HUE_CODES[round(h)-1]
        if s % 1 == 0:
            s = int(s)
        return '{0}-{1}-{2}s'.format(hue_name, l, s)
    else:
        if h == s == 0.0:
            if l >= 9.5:
                return 'W'
            if l <= 1.5:
                return 'Bk'
            return 'Gy-{0}'.format(l)
        t = pccs_lightness_and_saturation_to_tone(pccs_spec)
        if h % 1 == 0:
            h = int(h)
        return '{0}{1}'.format(t, h)


def pccs_colour_to_munsell_colour(pccs_colour: str) -> str:
    """Convert given PCCS color to Munsell color

    >>> pccs_colour_to_munsell_colour('v1')
    '10.0RP 4.5/13.5'
    >>> pccs_colour_to_munsell_colour('v2')
    '4.0R 4.5/13.0'
    >>> pccs_colour_to_munsell_colour('Gy-5.5')
    'N5.5'
    >>> pccs_colour_to_munsell_colour('W')
    'N9.5'
    >>> pccs_colour_to_munsell_colour('Bk')
    'N1.5'
    >>> pccs_colour_to_munsell_colour('12:G-5.5-9s')
    '3.0G 5.5/12.0'
    >>> pccs_colour_to_munsell_colour('2:R-8.5-2s')
    '4.0R 8.5/2.0'
    >>> pccs_colour_to_munsell_colour('n-5.5')
    'N5.5'
    >>> pccs_colour_to_munsell_colour('n-9.5')
    'N9.5'
    >>> pccs_colour_to_munsell_colour('n-1.5')
    'N1.5'
    """

    pccs_spec = pccs_colour_to_pccs_specification(pccs_colour)
    munsell_spec = pccs_specification_to_munsell_specification(pccs_spec)
    munsell_spec = normalize_munsell_specification(munsell_spec)
    return munsell_specification_to_munsell_colour(munsell_spec)


def munsell_colour_to_pccs_colour(munsell_colour: str, *,
                                  long: bool = False) -> str:
    """Convert given Munsell color to PCCS color

    >>> munsell_colour_to_pccs_colour('3.0G 5.5/12.0')
    'v12'
    >>> munsell_colour_to_pccs_colour('3.5R 8.5/2.0')
    'p2'
    >>> munsell_colour_to_pccs_colour('N5.5')
    'Gy-5.5'
    >>> munsell_colour_to_pccs_colour('N9.5')
    'W'
    >>> munsell_colour_to_pccs_colour('N1.5')
    'Bk'
    >>> munsell_colour_to_pccs_colour('3.0G 5.5/12.0', long=True)
    '12:G-5.5-9s'
    >>> munsell_colour_to_pccs_colour('3.5R 8.5/2.0', long=True)
    '2:R-8.5-2s'
    >>> munsell_colour_to_pccs_colour('N5.5', long=True)
    'n-5.5'
    >>> munsell_colour_to_pccs_colour('N9.5', long=True)
    'n-9.5'
    >>> munsell_colour_to_pccs_colour('N1.5', long=True)
    'n-1.5'
    """

    munsell_spec = munsell_colour_to_munsell_specification(munsell_colour)
    pccs_spec = munsell_specification_to_pccs_specification(munsell_spec)
    return pccs_specification_to_pccs_colour(pccs_spec, long=long)


if __name__ == '__main__':
    import doctest
    doctest.testmod()
