"""PCCS and Munsell conversion implementation based on the easy
algorythm
"""


__all__ = [
    'pccs_specification_to_munsell_specification',
    'munsell_specification_to_pccs_specification',
    'tilde_C', 'tilde_lambda',
]


from colour.notation.munsell import normalize_munsell_specification

import numpy as np
import numpy.typing as npt
from numpy import e, pi, sin, cos

from sympy import Symbol, solve


from .utils import (munsell_linear_hue_scale_to_hue_step_and_code,
                    munsell_hue_step_and_code_to_linear_hue_scale,
                    round_align)


def pccs_specification_to_munsell_specification(pccs_spec: npt.ArrayLike
                                                ) -> npt.NDArray:
    """Convert PCCS intermediate specification to Munsell color system
    intermediate specification.

    Parameters
    ----------
    pccs_spec: array_like
        PCCS specification

    Returns
    -------
    munsell_spec: ndarray
        Munsell specification


    PCCS specification is a sequence of three float values:

    h (float) [1-24]
        Hue. '1:pR' to '24:RP'.
    l (float) [1.5-9.5]
        Lightness. It is identical to Value in the Munsell system.
    s (float) [0-10]
        Saturation.

    Munsell specification (on the munsell library) is a sequence of
    four float values:

    step (float) [1-10]
        Sub-step hue value for the letter code. The value is named `hue`
        in the original library code.
    V (float) [0-10]
        Value. It is identical to Lightness in the PCCS.
    C (float) [0-14]
        Chroma. Represents purity of a color.
    code (float) [1-10]
        Hue letter code. 1 for 'B', 2 for 'BG', 3 for 'G', etc.

    >>> pccs_specification_to_munsell_specification([12.0, 5.5, 9.0])
    array([  3. ,   5.5,  12. ,   3. ])
    >>> pccs_specification_to_munsell_specification([2.0, 8.5, 2.0])
    array([ 4. ,  8.5,  2. ,  7. ])
    """

    h, l, s = pccs_spec

    V = l

    x = (h - 1) / 12 * pi
    H = (100 / (2 * pi) * x - 1.0
         + 0.12 * cos(x) + 0.34 * cos(2 * x) + 0.40 * cos(3 * x)
         - 2.7 * sin(x) + 1.5 * sin(2 * x) - 0.40 * sin(3 * x))
    H = round_align(H, 1.0)

    C = (tilde_C(h)
         * (0.077 * s + 0.0040 * s ** 2)
         * (1 - e ** (-tilde_lambda(h) * l)))
    C = round_align(C, 0.5)

    m_hue, m_code = munsell_linear_hue_scale_to_hue_step_and_code(H)
    munsell_spec = np.array([m_hue, V, C, m_code])
    munsell_spec = normalize_munsell_specification(munsell_spec)
    return munsell_spec


def munsell_specification_to_pccs_specification(munsell_spec: npt.ArrayLike
                                                ) -> npt.NDArray:
    """Convert Munsell color system intermediate specification values
    to PCCS intermediate specification values.

    Parameters
    ----------
    munsell_spec: array_like
        Munsell specification.

    Returns
    -------
    ndarray
        PCCS specification array.


    This is the opposite version of
    :func:`pccs_specification_to_munsell_specification` function.

    >>> munsell_specification_to_pccs_specification([3.0, 5.5, 12.0, 3.0])
    array([ 12. ,   5.5,   9. ])
    >>> munsell_specification_to_pccs_specification([3.5, 8.5, 2.0, 7.0])
    array([ 2. ,  8.5,  2. ])
    """

    step, V, C, code = munsell_spec

    # Note: `np.nan` returns `False` when it is compared to itself.
    step = step if step == step else 0.0
    V = V if V == V else 0.0
    C = C if C == C else 0.0
    code = code if code == code else 0.0

    l = V

    if step == C == code == 0.0:
        h = 0.0
        s = 0.0
    else:
        H = munsell_hue_step_and_code_to_linear_hue_scale(step, code)
        y = H / 50 * pi
        h = ((24 / (2 * pi)) * y + 1.24
             + 0.020 * cos(y) - 0.10 * cos(2 * y) - 0.11 * cos(3 * y)
             + 0.68 * sin(y) - 0.30 * sin(2 * y) + 0.013 * sin(3 * y))

        s_ = Symbol('s')
        solutions = solve(0.0040 * s_ ** 2
                          + 0.077 * s_
                          - C / (tilde_C(h) * (1 - e ** (-tilde_lambda(h) * V))))
        s = float(max(solutions))

    pccs_spec = np.array([h, l, s])
    return round_align(pccs_spec, 0.5)


def tilde_C(h: float) -> float:
    """(internal) The formula (4) on the reference paper. """

    return 12 + 1.7 * sin((h + 2.2 / 12 * pi))


def tilde_lambda(h: float) -> float:
    """(internal) The formula (2) on the reference paper. """

    return 0.81 - 0.24 * sin((h - 2.6) / 12 * pi)


if __name__ == '__main__':
    import doctest
    doctest.testmod()
