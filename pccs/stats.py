from colour.notation.munsell import munsell_colour_to_munsell_specification

from . import (pccs_colour_to_pccs_specification,
               pccs_specification_to_munsell_specification)
from .utils import (munsell_hue_step_and_code_to_linear_hue_scale)
from .dataset import PCCS_COLOUR_SAMPLE_INDEX


def main():
    for A, (B, C) in PCCS_COLOUR_SAMPLE_INDEX.items():
        sample_pccs_colour = A
        sample_pccs_colour_long = B
        sample_munsell_colour = C

        sample_munsell_spec = munsell_colour_to_munsell_specification(
            sample_munsell_colour)
        sample_hue, sample_value, sample_chroma, sample_code = sample_munsell_spec
        sample_H = munsell_hue_step_and_code_to_linear_hue_scale(
            sample_hue, sample_code)

        sample_pccs_spec = pccs_colour_to_pccs_specification(
            sample_pccs_colour)
        munsell_spec = pccs_specification_to_munsell_specification(
            sample_pccs_spec)
        hue, value, chroma, code = munsell_spec
        H = munsell_hue_step_and_code_to_linear_hue_scale(hue, code)

        diff_H = abs(sample_H - H)
        diff_chroma = abs(sample_chroma - chroma)
        print(('{0} {{ dH: {1}, dC: {2} }}'
               .format(sample_pccs_colour, diff_H, diff_chroma)))



if __name__ == '__main__':
    main()
