"""Utility functions for PCCS colour manupilation."""


__all__ = ['munsell_linear_hue_scale_to_hue_step_and_code',
           'munsell_hue_step_and_code_to_linear_hue_scale',
           'round_align']


import numpy as np
import numpy.typing as npt


def munsell_linear_hue_scale_to_hue_step_and_code(munsell_linear_hue: float
                                                  ) -> tuple[float, float]:
    """(internal) Convert Munsell linear hue scale to step and code hue
    notation

    Parameters
    ----------
    munsell_linear_hue: float
        Linear hue scale [0-100]

    Returns
    -------
    munsell_hue_step: float
        Munsell hue step [1-10]
    munsell_hue_code: float
        Munsell hue code index [1-10]


    Convert 0-100 linear hue scale to 1-10 hue step and base letter
    code of Munsell color system in format of the intermediate
    specification used in the colour.notaion.musell library of the
    coulour-science package.


    Notes
    -----

    The linear hue scale which takes 0 to 100 is used in the
    specification of PCCS and the reference article. They use 0 (== 100)
    for 10RP, 10 for 10R, 20 for 10YR, etc.

    On the other hand, 10 steps of values and letter codes are the way
    to describe hue in the Munsell color system itself, but the order
    (indices) for the codes used in the intermediate specification of
    colour.notation.musell library are different from that in linear
    scale above. The indices of the hue codes are::

        1: B
        2: BG
        3: G
        4: GY
        5: Y
        6: YR
        7: R
        8: RP
        9: P
        10: PB

    Notice not only that they start counting with different hue codes
    but also they conunts the codes in different directions! PCCS specs
    count them clockwise, while The munsell library do counterclockwise.

    The function covers only the conversion of that above.

    >>> munsell_linear_hue_scale_to_hue_step_and_code(0.0)
    (10.0, 8.0)
    >>> munsell_linear_hue_scale_to_hue_step_and_code(4.0)
    (4.0, 7.0)
    >>> munsell_linear_hue_scale_to_hue_step_and_code(22.0)
    (2.0, 5.0)
    >>> munsell_linear_hue_scale_to_hue_step_and_code(43.0)
    (3.0, 3.0)
    >>> munsell_linear_hue_scale_to_hue_step_and_code(60.0)
    (10.0, 2.0)
    >>> munsell_linear_hue_scale_to_hue_step_and_code(83.0)
    (3.0, 9.0)
    """

    H1, H2 = divmod(munsell_linear_hue, 10.0)
    step = H2 % 10.0
    if step == 0.0:
        step = 10.0
        H1 -= 1.0
    code = (10.0 - H1 + 6.0) % 10.0 + 1.0
    return step, code


def munsell_hue_step_and_code_to_linear_hue_scale(munsell_hue_step: float,
                                                  munsell_hue_code: float
                                                  ) -> float:
    """(internal) Convert Munsell step and code hue notation to linear
    hue scale.

    Parameters
    ----------
    munsell_hue_step: float
        10 step of the hue. [1-10]
    munsell_hue_code: float
        Base hue code index. [1-10]

    Returns
    -------
    float
        0-100 of linear hue scale.


    This is the opposite version of
    :func:`munsell_linear_hue_scale_to_hue_step_and_code`. Read the
    notes for the function for the detail.

    >>> munsell_hue_step_and_code_to_linear_hue_scale(10.0, 8.0)
    0.0
    >>> munsell_hue_step_and_code_to_linear_hue_scale(4.0, 6.0)
    14.0
    >>> munsell_hue_step_and_code_to_linear_hue_scale(3.0, 4.0)
    33.0
    >>> munsell_hue_step_and_code_to_linear_hue_scale(10.0, 2.0)
    60.0
    >>> munsell_hue_step_and_code_to_linear_hue_scale(10.0, 1.0)
    70.0
    >>> munsell_hue_step_and_code_to_linear_hue_scale(3.0, 10.0)
    73.0
    >>> munsell_hue_step_and_code_to_linear_hue_scale(6.0, 8.0)
    96.0
    """

    hi = (10.0 - (munsell_hue_code - 1.0) + 6.0) % 10.0
    lo = munsell_hue_step
    return (hi * 10.0 + lo) % 100.0


def round_align(value: npt.ArrayLike, scale: float) -> float:
    """(internal) Round value to given alignment scale.

    >>> round_align(1.0, 0.5)
    1.0
    >>> round_align(1.4, 0.5)
    1.5
    >>> round_align(1.6, 0.5)
    1.5
    >>> round_align(1.8, 0.5)
    2.0
    >>> round_align(2.0, 2.5)
    2.5
    >>> round_align(6.0, 2.5)
    5.0
    >>> round_align(np.array([1.0, 1.4, 1.8]), 0.5)
    array([ 1. ,  1.5,  2. ])
    """
    return np.round(value / scale) * scale


if __name__ == '__main__':
    import doctest
    doctest.testmod()
